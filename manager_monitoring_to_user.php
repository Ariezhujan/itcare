               
			   <table id="lookup" class="table table-bordered table-hover table-striped">
			   <h5 class="text-center">PROGRESS REQUEST USER STATUS</h5>
			   <thead>
                    <tr>
                           <th>User Data</th>
                           <th>Progress</th>
                           <th>Responsible</th>
                           <th>Dateline</th>
                      </tr>
                  </thead>
                  <tbody>
<?php
	try{
	 include "koneksi.php";
	 date_default_timezone_set('Asia/Jakarta');
		$query = $db->prepare("SELECT * FROM adminto_user ORDER BY no DESC");
		$query->execute();
			while($data = $query->fetch(PDO::FETCH_OBJ)){ ?>
				      <tr>       
					        <td><h6><?php echo $data->request ?></h6></td>
					        <td><h6><?php echo $data->progress ?></h6></td>
					        <td><h6><?php echo $data->worker ?></h6></td>
					        <td><h6><?php echo $data->dateline ?></h6></td>
					  </tr>
			<?php ;}
		}catch (PDOException $e){
	    print "koneksi/query bermasalah: " . $e->getMessage() . "<br/>";
		$db = null;
}?> 
</tbody>
</table>