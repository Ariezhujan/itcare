<?php
session_start();
if(isset($_SESSION['data']) == true){
	header("location:/itcare/$_SESSION[data]/");
}else{
date_default_timezone_set('Asia/Jakarta');
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>itCARE</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
<link rel="stylesheet" href="css/hero-slider-style.css">
<link rel="stylesheet" href="css/templatemo-style.css">
<link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
 
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <h2>itCARE.</h2></div>
    
 
    <div class="" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li>
		 <form action="engine_login.php" method="post" class="form-group">
		   <input type="text" name="user" class="form-control" placeholder="username" required="required"/>
           <input type="password" name="key" class="form-control" placeholder="password" required="required"/>
		   <input type="submit" style="width:300px" class="btn btn-info input-lg " value="login"/>
		 </form>
		</li>
      </ul>
    </div>	
  </div>
</nav>

<header id="home">
  <div class="container intro">
    <div class="overlay">
        <section class="cd-hero">
            <ul class="cd-hero-slider autoplay"> 
                <li class="selected">
                    <div class="cd-full-width">
                                <h2 class="mark1">REQUEST</h2>
                                <p class="m-b-mid">About request for repairs</p>
                    </div> 
                </li>

                <li>
                    <div class="cd-full-width">
                                <h2 class="mark2">PROGRESS</h2>
                                <p>Status can be monitored</p> 
                    </div>
                </li>

                <li>
                    <div class="cd-full-width">
                                <h2 class="mark3">maintenance</h2>
                                <p class="tm-site-description">Periodic Device maintenance</p>
                    </div> 
                </li>
            </ul> 
        </section>
    </div>
  </div>
</header>

<div id="footer">
  <div class="container text-center ">
    <div class="col-md-8 col-md-offset-2">
      <p>&copy; 2018. <a href="#home" rel="nofollow">ARIS FAHUJAN</a></p>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/jquery.1.11.1.js"></script> 
<script src="js/hero-slider-script.js"></script> 
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<script>     
       
            $(document).ready(function(){

                /* Auto play bootstrap carousel 
                * http://stackoverflow.com/questions/13525258/twitter-bootstrap-carousel-autoplay-on-load
                -----------------------------------------------------------------------------------------*/                
                $('.carousel').carousel({
                  interval: 3000
                })

                /* Enable swiping carousel for tablets and mobile
                 * http://lazcreative.com/blog/adding-swipe-support-to-bootstrap-carousel-3-0/
                 ---------------------------------------------------------------------------------*/
                if($(window).width() <= 991) {
                    $(".carousel-inner").swipe( {
                        //Generic swipe handler for all directions
                        swipeLeft:function(event, direction, distance, duration, fingerCount) {
                            $(this).parent().carousel('next'); 
                        },
                        swipeRight: function() {
                            $(this).parent().carousel('prev'); 
                        },
                        //Default is 75px, set to 0 for demo so any distance triggers swipe
                        threshold:0
                    });
                }  

                /* Handle window resize */
                $(window).resize(function(){
                    if($(window).width() <= 991) {
                        $(".carousel-inner").swipe( {
                            //Generic swipe handler for all directions
                            swipeLeft:function(event, direction, distance, duration, fingerCount) {
                                $(this).parent().carousel('next'); 
                            },
                            swipeRight: function() {
                                $(this).parent().carousel('prev'); 
                            },
                            //Default is 75px, set to 0 for demo so any distance triggers swipe
                            threshold:0
                        });
                     }
                });                             
            });

        </script>          
</body>
</html>
<?php ;} ?>