<?php
session_start();
if($_SESSION['data'] == 'user'){
date_default_timezone_set('Asia/Jakarta');
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>itCARE</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css"  href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script type="text/javascript">
var otomatis = setInterval(
function ()
{
$('#watching_tabel').load('direct/monitoring_to_user.php').fadeIn("slow");
}, 1000)
</script>
</head>
<body>
</head>
<body data-spy="scroll" data-target=".navbar-fixed-top">
<nav id="menu" class="navbar navbar-default">
  <div class="container"> 
    <div class="navbar-header hh">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      Welcome User.</div>
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
		<li><a href="dashboard_user.php?#request" class="btn btn-info">Dashboard</a></li>
		<li><a href="#worked" class="btn btn-info">Monitoring</a></li> 
        <li><a href="/itcare/logout.php" class="btn btn-primary">logout</a></li> 
 </ul>
  </div>
  </div>
</nav>


  <div id="worked" class="container">
  <div class="intro">
  <div id="services">
  <h3 class="text-center">Progress Monitoring</h3>
  </div>
		    <div id="watching_tabel"></div>
  </br>
  </br>
  </br>
  </div>
  </div>
  </br>
  </br>
  </br>
  </br>
  </br>

<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
    <div class="col-md-8 col-md-offset-2">
      <p>&copy; 2018. <a href="#home" rel="nofollow">fahujanaris@gmail.com</a></p>
    </div>
  </div>
</div>
<script type="text/javascript" src="../js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="../js/bootstrap.js"></script> 
<script type="text/javascript" src="../js/main.js"></script>
       
</body>
</html>
<?php 
}else{
	header('location:/itcare/');
}
 ?>