<?php
session_start();
if($_SESSION['data'] == 'user'){
date_default_timezone_set('Asia/Jakarta');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>itCARE</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css"  href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../css/style.css">


</head>
<body>
</head>
<body data-spy="scroll" data-target=".navbar-fixed-top">
<nav id="menu" class="navbar navbar-default">
  <div class="container"> 
    <div class="navbar-header hh">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      Welcome User.</div>
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
		<li><a href="#request" class="btn btn-info">Dashboard</a></li>
		<li><a href="monitor_user.php#worked" class="btn btn-info">Monitoring</a></li>
        <li><a href="/itcare/logout.php" class="btn btn-primary">logout</a></li>
 </ul>
  </div>
  </div>
</nav>

  <div id="request" class="container">
  <div class="intro">
  <div id="services">
  <h3 class="text-center">Device Repair Service Form</h3>
  </div>
                                <form action="engine_input_user.php" method="post" role="search">
								<div class="row">
                                <div class="col-sm-4">
								</br>
								<label><h5>Id</h5></label>
                                    <div class="form-group">
                                        <input type="number" name="id" class="input-md" style="height:40px; width:360px;" required="required"/>
                                    </div>
								  <label><h5>Name</h5></label>
                                    <div class="form-group">
                                        <input type="text" name="user" class="form-control" placeholder="" required="required"/>
                                    </div>
								  <label><h5>Divisi</h5></label>
                                    <div class="form-group">
                                        <select name="divisi" class="input-md" style="height:40px; width:360px;" placeholder="" required="required">
										  <option value=""></option>
										  <option value="HRD">HRD</option>
										  <option value="Warehouse">Warehouse</option>
										  <option value="Produksi">Produksi</option>
										  <option value="Finance">Finance</option>
										</select>
                                    </div>
								 <label><h5>Device</h5></label>
                                    <div class="form-group">
                                        <select name="device" class="input-md" style="height:40px; width:360px;" placeholder="" required="required">
										  <option value=""></option>
										  <option value="Komputer">Komputer</option>
										  <option value="Laptop">laptop</option>
										  <option value="Printer">Printer</option>
										</select>
                                    </div>
                                    </div>
								  <div class="col-sm-8">
                                    <div class="form-group">
									<br>
                                        <textarea name="pesan" class="form-control" rows="5" cols="40" class="input-md " placeholder="Remarks description Abnormaly" required="required"></textarea>
                                    </div>
									<input type="submit" class="btn btn-info" value="send"/>
                                  </div>
                                  </div>
                                </form>
  </div>
  </div>

<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
    <div class="col-md-8 col-md-offset-2">
      <p>&copy; 2018. <a href="#home" rel="nofollow">fahujanaris@gmail.com</a></p>
    </div>
  </div>
</div>
<script type="text/javascript" src="../js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="../js/bootstrap.js"></script> 
<script type="text/javascript" src="../js/main.js"></script>
       
</body>
</html>
<?php
}else{
	header('location:/itcare/');
}
?>