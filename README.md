**ROOT Direktori itcare**
  **index.php** - Landing Page Login Untuk Menuju Akses user,admin,itsuport, dan manager.
  **engine_login.php** - Menangani inputan dari file *index.php* Dan Mengarah kan Ke Akses Yang Dituju,Dan terbentuknya Suatu session.
  **logout.php** - Menangani Logout/keluar Dari Seluruh Aksess, dan Menghancurkan suatu session.
  **koneksi.php** - sebagai file penghubung ke databased./ banyak file yang berkaitan dengan databased akan berkaitan dengan file ini.
   
   **Direktori css**
     **bootstrap.css** - Menangani layout dan Design Dari Seluruh Aplikasi itCare.
	 **hero-slider-style.css** - Menangani layout dan Design Dari *index.php*.
	 **templatemo-style.css** - Menangani layout dan Design Dari *index.php*.
	 **style.css** - Custom layout dan Design Dari Seluruh Aplikasi itCare.
**	 
   **Direktori js**
	 **bootstrap.js** - Menangani Interaksi Dari Seluruh Aplikasi itCare.
	 **hero-slider-script.js** - Menangani Interaksi Dari *index.php*.
	 **jquery.1.11.1.js** - Menangani Interaksi Dari Seluruh Aplikasi itCare.
	 **main.js** - Menangani Interaksi Dari Seluruh Aplikasi itCare.
	  
   **Direktori user**
       **index.php** - sebagai Penghubung aksess login User Untuk di direct ke *dashboard_user.php*. file index1.php wajib di rename menjadi index.php saja.
	   **dashboard_user.php** - sebagai Request Perangkat yang rusak oleh Karyawan.
	   **monitor_user.php** - sebagai Monitorng progress Untuk karyawan.
	   **engine_input_user.php** - sebagai Penerima Inputan dari *dashboard_user.php*, dan akan disimpan di tabel user di database.
	     **Child Direktori direct**
	     **send_admin_monitoring_to_user.php** - sebagai Pengambil Data Dari Databased, Yang akan di tampilkan Di Halaman *dashboard_user.php*.
		 **koneksi.php** - sebagai file penghubung ke databased./ banyak file yang berkaitan dengan databased akan berkaitan dengan file ini.</b>

   **Direktori admin**
     **index.php** - sebagai Penghubung aksess login User Untuk di direct ke *dashboard_user.php*. file index2.php wajib di rename menjadi *index.php* saja.
     **dashboard_admin.php** - Sebagai monitoring Request Masuk Dari User Berupa Informasi Kerusakan Perangkat.
	 **form_admin_input_itsuport.php** - Mengirim informasi request ke IT suport,  dan juga menampilkan tabel informasi request yang dikirimkan ke IT suport.
	 **form_admin_update_status.php** - Mengirim informasi Status Progress ke User,menampilkan tabel informasi request yang di kirimkan ke User, dan Menampilkan Progress Informasi Dari ItSuport.
	 **engine_input_admin_itsuport.php** - sebagai Penerima Inputan dari *form_admin_input_itsuport.php*, dan akan disimpan di tabel user di database.
	 **engine_input_admin_update.php** - sebagai Penerima Inputan dari *form_admin_update_status.php*, dan akan disimpan di tabel user di database.
	 **koneksi.php** - sebagai file penghubung ke databased./ banyak file yang berkaitan dengan databased akan berkaitan dengan file ini.
	   **Child Direktori direct**
	     **send_admin_monitoring_to_user.php** - sebagai Pengambil Data Dari Databased, Yang akan di tampilkan Di Halaman *dashboard_user.php*.
		 **delete_sent_to_user.php** - Delete Data Dari *send_admin_monitoring_to_user.php*, Yang akan di tampilkan Di Halaman *dashboard_user.php*.
		 **send_monitoring_itsuport_to_admin.php** - sebagai Pengambil Data Dari Databased, Yang akan di tampilkan Di Halaman *form_admin_update_status.php*
		 **delete_progress_itsuport.php** - Delete Data Dari *send_monitoring_itsuport_to_admin.php*, Yang akan di tampilkan Di Halaman *form_admin_update_status.php*.
		 **send_monitoring_to_itsuport.php** - sebagai Pengambil Data Dari Databased, Yang akan di tampilkan Di Halaman *form_admin_input_itsuport.php*.
		 **delete_admin_sent_to_itsuport.php** - Delete Data Dari *send_monitoring_to_itsuport.php*, Yang akan di tampilkan Di Halaman *form_admin_input_itsuport.php*.
		 **send_user_monitoring.php** - sebagai Pengambil Data Dari Databased, Yang akan di tampilkan Di Halaman *dashboard_admin.php*.
		 **delete_user.php** - Delete Data Dari *send_user_monitoring.php*, Yang akan di tampilkan Di Halaman *dashboard_admin.php*.
		 **koneksi.php** - sebagai file penghubung ke databased./ banyak file yang berkaitan dengan databased akan berkaitan dengan file ini.
		 
   **Direktori itsuport**
     **index.php** - sebagai Penghubung aksess login User Untuk di direct ke *dashboard_itsuport.php*. file *index3.php* wajib di rename menjadi *index.php* saja.
     **dashboard_itsuport.php** - Sebagai monitoring data Perbaikan Perangkat It, dan yang mengirim dari Admin. juga Terdapat Status Server.
	 **form_itsuport.php** - Form Sebagai Input Progreess, dan Di kirim ke Admin Dan Manager.
	 **engine_form_itsuport.php** - File Yang Mengelola Inputan dari*form_itsuport.php* dan Menyimpanya di databased.
	 **koneksi.php** - sebagai file penghubung ke databased./ banyak file yang berkaitan dengan databased akan berkaitan dengan file ini.
       **Child Direktori direct**
	     **monitoring_to_itsuport.php** - sebagai Pengambil Data Dari Databased, Yang akan di tampilkan Di Halaman *dashboard_itsuport.php*.
         **send_monitoring_to_admin.php** - sebagai Pengambil Data Dari Databased, Yang akan di tampilkan Di Halaman *form_itsuport.php*.
		 **koneksi.php** - sebagai file penghubung ke databased.banyak file yang berkaitan dengan databased akan berkaitan dengan file ini.
**		 
    **Direktori manager**
	  **index.php** - sebagai Penghubung aksess login User Untuk di direct ke *dashboard_manager.php*. file *index4.php* wajib di rename menjadi *index.php* saja.
	  **dashboard_manager.php** - Monitoring Dari Progress ItSuport Dan Menampilkan Status Server.
	    **Child Direktori direct**
		**manager_monitoring_to_user.php** - Menampilkan Progress Dari Status User. dan Di tampilkan di page *dashboard_manager.php*.
		**monitoring_itsuport_to_admin.php** - Menampilkan Progress Dari Status IT suport. dan Di tampilkan di page *dashboard_manager.php*.         
		****koneksi.php** - sebagai file penghubung ke databased.banyak file yang berkaitan dengan databased akan berkaitan dengan file ini.
		
 		
**NOTE** - File itcare.sql Import to Databased itcare.
         - **Login**
		     *user* - user : user , password : user1234.
			 *admin* - user : admin , password : admin1234.
			 *itsuport* - user : itsuport , password : itsuport1234.
			 *manager* - user : manager , password : manager1234.
		 
		 
**Description : menerima laporan kerusakan dan memperbaiki Perangkat IT serta memberikan progress status perbaikan ke pihak pihak yang berkepentingan**