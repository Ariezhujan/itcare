-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 27, 2018 at 11:41 AM
-- Server version: 5.5.34
-- PHP Version: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `itcare`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminto_itsuport`
--

CREATE TABLE IF NOT EXISTS `adminto_itsuport` (
  `no` bigint(50) NOT NULL AUTO_INCREMENT,
  `user_request` varchar(100) NOT NULL,
  `name_teknisi` varchar(20) NOT NULL,
  `dateline` date NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `adminto_itsuport`
--

INSERT INTO `adminto_itsuport` (`no`, `user_request`, `name_teknisi`, `dateline`) VALUES
(1, '1145<br>Gunawan Sutanto<br>Printer<br>Produksi<br>Saat cetak tidak keluar hasilnya', 'Danang Prasetyo', '2018-10-31'),
(2, '7362<br>Adi KArya<br>Laptop<br>HRD<br>Request Laptop baru Mas..', 'Cahyo', '2018-10-26');

-- --------------------------------------------------------

--
-- Table structure for table `adminto_user`
--

CREATE TABLE IF NOT EXISTS `adminto_user` (
  `no` bigint(50) NOT NULL AUTO_INCREMENT,
  `request` varchar(100) NOT NULL,
  `progress` varchar(20) NOT NULL,
  `worker` varchar(30) NOT NULL,
  `dateline` date NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `adminto_user`
--

INSERT INTO `adminto_user` (`no`, `request`, `progress`, `worker`, `dateline`) VALUES
(1, '7362<br>Adi KArya<br>Laptop<br>HRD<br>Request Laptop baru Mas..<br>Cahyo<br>2018-10-26', 'Waiting', 'Cahyo', '2018-10-26'),
(2, '1145<br>Gunawan Sutanto<br>Printer<br>Produksi<br>Saat cetak tidak keluar hasilnya<br>Danang Prasety', 'Work In Progress', 'Danang Prasetyo', '2018-10-31');

-- --------------------------------------------------------

--
-- Table structure for table `itsuport_toadmin`
--

CREATE TABLE IF NOT EXISTS `itsuport_toadmin` (
  `no` bigint(50) NOT NULL AUTO_INCREMENT,
  `data_user` varchar(100) NOT NULL,
  `progress` varchar(20) NOT NULL,
  `remarks` varchar(100) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `itsuport_toadmin`
--

INSERT INTO `itsuport_toadmin` (`no`, `data_user`, `progress`, `remarks`) VALUES
(1, '1145<br>Gunawan Sutanto<br>Printer<br>Produksi<br>Saat cetak tidak keluar hasilnya<br>Danang Prasety', 'Work In Progress', 'pergantian card reader dengan yang baru');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `no` int(50) DEFAULT NULL,
  `user` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`user`),
  UNIQUE KEY `no` (`no`,`password`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`no`, `user`, `password`) VALUES
(1, 'user', 'user1234'),
(2, 'admin', 'admin1234'),
(3, 'itsuport', 'itsuport1234'),
(4, 'manager', 'manager1234');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `no` bigint(50) NOT NULL AUTO_INCREMENT,
  `id` int(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `divisi` varchar(50) NOT NULL,
  `device` varchar(50) NOT NULL,
  `pesan` varchar(100) NOT NULL,
  `tgl` date NOT NULL,
  `waktu` time NOT NULL,
  PRIMARY KEY (`no`),
  KEY `id` (`id`,`nama`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`no`, `id`, `nama`, `divisi`, `device`, `pesan`, `tgl`, `waktu`) VALUES
(1, 3577, 'Aris Fahujan', 'Warehouse', 'Komputer', 'perangkat tidak mau hidup', '2018-10-27', '14:13:54'),
(3, 24243, 'Aldo ananda Fahujan', 'HRD', 'Laptop', 'Sering Mati Sendiri', '2018-10-27', '14:15:20'),
(4, 34321, 'Indah Putri Cahyani', 'HRD', 'Printer', 'Untuk ngeprint sering tidak maksimal ', '2018-10-27', '14:16:31'),
(5, 3622, 'Sucipto', 'Finance', 'Komputer', 'Mas, Komputer blue screeenn Help!!', '2018-10-27', '14:17:21'),
(6, 7653, 'Puguh Susanto', 'Produksi', 'Komputer', 'Tiba tiba mati sendiri mas', '2018-10-27', '14:18:16');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
