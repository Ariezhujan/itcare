<?php
session_start();
if($_SESSION['data'] == 'itsuport'){
date_default_timezone_set('Asia/Jakarta');
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>itCARE</title>
<meta name="description" content="">
<meta name="author" content="">

<link rel="stylesheet" type="text/css"  href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script type="text/javascript">
var otomatis = setInterval(
function ()
{
$('#watching_tabel').load('direct/monitoring_to_itsuport.php').fadeIn("slow");
}, 1000)
</script>

</head>
<body>
</head>
<body data-spy="scroll" data-target=".navbar-fixed-top">
<nav id="menu" class="navbar navbar-default">
  <div id="request" class="container"> 
    <div class="navbar-header hh">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      Welcome IT Suport.</div>
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#request" class="btn btn-info">Dashboard</a></li>
        <li><a href="form_itsuport.php?#worked" class="btn btn-info">Input Progress</a></li>
        <li><a href="/itcare/logout.php" class="btn btn-primary">logout</a></li>
      </ul>
      </div>
   </div>
</nav>


  <div class="container">
  <div class="row">
  <div class="intro">
  <div id="services">
  <h3 class="text-center">Data From Admin</h3>
  </div>
  <div class="col-md-4">
  <h5>Server Setatus</h5>
  <iframe width="100%" height="400px" src="https://server-status-tsp.firebaseapp.com/status"></iframe>
  </div>
  <div class="col-md-8">
  <div id="watching_tabel"></div>
  </div>
  </div>
  </div>
  </div>
  </br>
  </br>
  </br>
  </br>
  </br>

<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
    <div class="col-md-8 col-md-offset-2">
      <p>&copy; 2018. <a href="#home" rel="nofollow">fahujanaris@gmail.com</a></p>
    </div>
  </div>
</div>
<script type="text/javascript" src="../js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="../js/bootstrap.js"></script> 
<script type="text/javascript" src="../js/main.js"></script>
       
</body>
</html>
<?php
}else{
	header('location:/itcare/');
}
?>