<?php
session_start();
if($_SESSION['data'] == 'admin'){
date_default_timezone_set('Asia/Jakarta');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>itCARE</title>
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css"  href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<script type="text/javascript">
var otomatis = setInterval(
function ()
{
$('#watching_tabel').load('direct/send_admin_monitoring_to_user.php').fadeIn("slow");
$('#watching_tabel_progress_itsuport').load('direct/send_monitoring_itsuport_to_admin.php').fadeIn("slow");
}, 1000)
</script>
</head>
<body>
</head>
<body data-spy="scroll" data-target=".navbar-fixed-top">
<nav id="menu" class="navbar navbar-default">
  <div id="worked" class="container"> 
    <div class="navbar-header hh">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      Welcome Admin.</div>
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="dashboard_admin.php?#request" class="btn btn-info">Dashboard</a></li>
        <li><a href="form_admin_input_itsuport.php?#worked" class="btn btn-info">Input For IT Support</a></li> 
        <li><a href="#worked" class="btn btn-info">Input Progress To User</a></li> 
        <li><a href="/itcare/logout.php" class="btn btn-primary">logout</a></li> 
 </ul>
  </div>
  </div>
</nav>


  <div class="container-fluid">
  <div class="intro">
  <div id="services">
  <h3 class="text-center">Status Update For User</h3>
  </div>
                              <form action="engine_input_admin_update.php" method="post" role="search">
								<div class="row">
                                <div class="col-sm-4">
								<label><h5>Select Request</h5></label>
                                    <div class="form-group">
                                        <select name="request" id="idtype" class="input-md" style="height:40px; width:360px;" placeholder="Search" required="required">
										  <option ></option>
										   <?php 
                                              try{
	                                            include "koneksi.php";
	                                            date_default_timezone_set('Asia/Jakarta');
	                                            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	                                             }catch (PDOException $e){
		                                           print "koneksi/query bermasalah: " . $e->getMessage() . "<br/>";
		                                           $db = null;
		                                         }	
		                                        $query = $db->prepare("SELECT * FROM adminto_itsuport ORDER BY no DESC");
		                                        $query->execute();
		                                         while($data = $query->fetch(PDO::FETCH_OBJ)){
			                                     echo "<option value='".$data->user_request ."<br>". $data->name_teknisi ."<br>". $data->dateline . "'>$data->user_request $data->name_teknisi $data->dateline </option>";
			                                   }
                                              ?>
										</select>
                                    </div>
								  <label><h5>Status Update</h5></label>
                                    <div class="form-group">
                                        <select name="progress" class="input-md" style="height:40px; width:360px;" placeholder="Search "required="required">
										  <option ></option>
										  <option value="Waiting">Waiting</option>
										  <option value="Work In Progress">Work In Progress</option>
										  <option value="Done">Done</option>
										</select>
                                    </div>
								  <label><h5>Select Who Works</h5></label>
                                    <div class="form-group">
                                        <select name="worker" class="input-md" style="height:40px; width:360px;" placeholder="Search" required="required">
										  <option ></option>
										  <option value="Agus Setiawan">Agus Setiawan</option>
										  <option value="Danang Prasetyo">Danang Prasetyo</option>
										  <option value="Cahyo">Cahyo</option>
										</select>
                                    </div>
									<label><h5>DateLine</h5></label>
                                    <div class="form-group">
									   <input type="date" name="tgl" class="input-md" style="height:40px; width:360px;" placeholder="" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" class="btn btn-info" value="send"/>
                                    </div>
								   </form>
                                </div>
				    	<div class="col-sm-4">
                        <div id="watching_tabel"></div>
						</div>
						<div class="col-sm-4">
                            <div id="watching_tabel_progress_itsuport"></div>
						</div>
						</div>
  
  </div>
  </div>
  </br>
  </br>

<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
    <div class="col-md-8 col-md-offset-2">
      <p>&copy; 2018. <a href="#home" rel="nofollow">fahujanaris@gmail.com</a></p>
    </div>
  </div>
</div>
<script type="text/javascript" src="../js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="../js/bootstrap.js"></script> 
<script type="text/javascript" src="../js/main.js"></script>
       
</body>
</html>
<?php
}else{
	header('location:/itcare/');
}
?>